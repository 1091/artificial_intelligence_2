global domains 
	symbols = symbol*
	chars = char*
predicates
	fronttoken_cyr(string,string,string)	% ��������� ��������� �� ������� ����������� 
	razd(char)				% �����������
	symbol_counter(string,integer)		% ������� �������� � ������ �� ����� ������, ���� ���������� �����������
	del_front_space(string,string)		% �������� ����������� � ������ ������ 
	upper_lower_cyr(string,string)		% �������� upper_lower ��� �������� Windows
	upper_lower_cyr_convers(chars,chars)
	str_char_list(string,chars)		% �������������� ������ � ������ �������� 
	pack(chars,string)			% ����������� ������ �������� � ������ 
	convers(string,symbols)			% ���������������� ������� �������������� ������ � ������ ���� 
clauses
	% ��������� ��������� �� ������� ����������� 
	fronttoken_cyr(Str, Token, Rest_of_string):-
		symbol_counter(Str, Number),
		frontstr(Number,Str, Token,Rest_of_string).
	% �����������
	razd('\32').
	razd('\10').
	razd('\13').
	razd('\33').
	razd('\34').
	razd('\35').
	razd('\36').
	razd('\40').
	razd('\41').
	razd('\44').
	razd('\45').
	razd('\46').
	razd('\59').
	% ������� �������� � ������ �� ����� ������, ���� ���������� �����������
	symbol_counter("", 0).
	% ����������� � ������
	symbol_counter(Str, 0):-
		razd(Ch),
		frontchar(Str, Ch,_),
		!.
	symbol_counter(Str,Number):-
		frontchar(Str, _Char, Rest_of_string),
		symbol_counter(Rest_of_string, Number1),
		Number=Number1+1.
	% �������� ����������� � ������ ������ 
	del_front_space("","").
	del_front_space(Arg, Res):-
		frontchar(Arg, Char, Res1), razd(Char), !,
		del_front_space(Res1, Res).
		del_front_space(Arg, Arg):- frontchar(Arg, Char, _),
		not(razd(Char)).
	% �������� upper_lower ��� �������� Windows 
	upper_lower_cyr(InString, OutString):-
		str_char_list(InString,Char_List_for_InString),
		upper_lower_cyr_convers(Char_List_for_InString, Char_List_for_OutString),
		pack(Char_List_for_OutString,OutString).
		upper_lower_cyr_convers([],[]).
	upper_lower_cyr_convers([Char|Char_List],[Char1|Char_List1]):-
		char_int(Char,ASCII_code),
		ASCII_code>=192,ASCII_code<=223,!,
		ASCII_code_new=ASCII_code+32,
		char_int(Char1,ASCII_code_new),
		upper_lower_cyr_convers(Char_List,Char_List1).
	upper_lower_cyr_convers([Char|Char_List],[Char1|Char_List1]):-
		char_int(Char,ASCII_code),
		ASCII_code=168,!,
		ASCII_code_new=ASCII_code+16,
		char_int(Char1,ASCII_code_new),
		upper_lower_cyr_convers(Char_List,Char_List1).
	upper_lower_cyr_convers([Char|Char_List],[Char1|Char_List1]):-
		char_int(Char,ASCII_code),
		ASCII_code>=65, ASCII_code<=90, !,
		ASCII_code_new=ASCII_code+32,
		char_int(Char1,ASCII_code_new),
		upper_lower_cyr_convers(Char_List,Char_List1).
	upper_lower_cyr_convers([Char|Char_List],[Char|Char_List1]):-
		upper_lower_cyr_convers(Char_List, Char_List1).
	% �������������� ������ � ������ �������� 
	str_char_list("",[]).
	str_char_list(Word,[Char|Char_List]):-
		frontchar(Word,Char,WordRest),
		str_char_list(WordRest,Char_List).
	% ����������� ������ �������� � ������ 
	pack([ ],"").
	pack([H|T], Res):-
		str_char(Str_H, H),
		pack(T, Res1),
		concat(Str_H, Res1, Res).
	% ���������������� ������� �������������� ������ � ������ ���� 
	convers("",[ ]):- !.
	convers(Str, [Head1|Tail]):-
		fronttoken_cyr(Str, Head, Str2),
		upper_lower_cyr(Head, Head1),
		del_front_space(Str2, Str1),
		convers(Str1, Tail).