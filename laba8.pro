include "laba8.inc"
include "laba8.con"
include "strings.pro"
include "logic.pro"
predicates
	task_win_eh : EHANDLER			%���������� ������� ����
clauses
	task_win_eh(_Win,e_Create(_),0):- !,	%������� �������� ����
	dlg_input_Create(_Win),			%�������� ����������� ����
	win_Destroy(_Win),!.			%����������� �������� ����
	task_win_eh(_,_,_):-!,fail.
goal
	%������� goal
	vpi_SetAttrVal(attr_win_3dcontrols,b_true),
	vpi_Init(
		[wsf_Border, wsf_TitleBar, wsf_Close, wsf_Maximize,wsf_Minimize],
		task_win_eh,
		no_menu,
		"����������� �2",
		"����������� �2"
	).
predicates
	dlg_input_eh : EHANDLER		%���������� ������� ����������� ����
	keywords(symbols,lst,lst)	%������ �� �������� ������
clauses
	dlg_input_Create(Parent):-
		win_CreateResDialog(Parent,wd_Modal,idd_input,dlg_input_eh,0).

	dlg_input_eh(_Win,e_Control(idc_ok,_CtrlType,_CtrlWin,_CtrlInfo),0):-
		!,					%������� ��
		Han=win_GetCtlHandle(_Win,idc_edit),	%���������� ���������� ����
        	S=win_GetText(Han), 			%��������� �������� �� ���������� ����
		convers(S,List),			%��������� ������ �� �����
		keywords(List,[],[]),			%������ �������� ����
		win_Destroy(_Win),			%�������� ����
		!.
	dlg_input_eh(_Win,e_Control(idc_cancel,_CtrlType,_CtrlWin,_CtrlInfo),0):-
		!,
		win_Destroy(_Win),
		!.
	dlg_input_eh(_,_,_):-
		!, fail.
	%����� �� �������� ������
	keywords(["������","fb2"|T],Y,N):-
  		keywords(T,[1|Y],N), !.
	keywords(["������","pdf"|T],Y,N):-
		keywords(T,[2|Y],N), !.
	keywords(["������","epub"|T],Y,N):-
  		keywords(T,[3|Y],N), !.
	keywords(["����", "������", "�������������"|T],Y,N):-
		keywords(T,[4|Y],N), !.
	keywords(["����", "������", "����������"|T],Y,N):-
		keywords(T,Y,[4|N]), !.
	keywords(["��������", "�����", "��������", "�������������"|T],Y,N):-
		keywords(T,[5|Y],N), !.
	keywords(["��������", "�����", "��������", "����������"|T],Y,N):-
		keywords(T,Y,[5|N]), !.
	keywords(["����������", "������","�����������"|T],Y,N):-
		keywords(T,[6|Y],N), !.
	keywords(["����������", "������","���������"|T],Y,N):-
		keywords(T,Y,[6|N]), !.
	keywords(["�������"|T],Y,N):-
		keywords(T,[7|Y],N), !.
	keywords(["����������"|T],Y,N):-
		keywords(T,Y,[7|N]), !.
	keywords(["�������"|T],Y,N):-
		keywords(T,[8|Y],N), !.
	keywords(["������������", "������", "������", "iOS"|T],Y,N):-
		keywords(T,[9|Y],N), !.
	keywords([],Y,N):-
		qv(1,Y,N), !.
	keywords([_|T],Y,N):-
		keywords(T,Y,N),!.