include "laba8.inc"
domains
	lst = integer*
predicates
	prop(integer,string)		% �������� ��������
	objects(integer,string,lst)	% ������� ���������� �������
	qv(integer,lst,lst)		% ������� ������������ ��������
	next(integer,lst,lst,integer)	% ���������� ������ ������������ �� �������� ������� 
	ex_in_lst(integer,lst)		% �������� �� ������� �������� � ������
	rvn(lst,lst)			% �������� ������������, ��� ��� �������� 1-�� ������ ���������� �� ������
clauses
	%�������� ��������
	prop(1,"1) ������ fb2?").
	prop(2,"2) ������ pdf?").
	prop(3,"3) ������ epub?").
	prop(4,"4) ������������� ���� ������?").
	prop(5,"5) ������������� �������� ����� ��������?").
	prop(6,"6) ������������ ���������� ������?").
	prop(7,"7) ��������� ����������?").
	prop(8,"8) ������� ����������� �������� ����?").
	prop(9,"9) ��������� ������ ������ iOS?").
	%������� ���������� �������
	objects( 1, "uBooks xl",	[1,2,3,4,6,9]).
	objects( 2, "ShortBook",	[1,4,6,9]).
	objects( 3, "���������",	[1,7,8,9]).
	objects( 4, "Bookmate",		[1,3,7,8,9]).
	objects( 5, "Kobo",		[1,3,6,7,8]).
	objects( 6, "Bluefire Reader",	[1,3,4,6,8,8,9]).
	objects( 7, "ShuBook SE",	[1,2,3,4,5,6,8,9]).
	objects( 8, "Exlibris FB2",	[1,3,5,6,7,8]).
	objects( 9, "iBooks",		[2,3,7,8]).
	objects(10, "Marvin for iOS",	[3,4,5,6,8,9]).
	%������� ������������ ��������
	qv(10,_,_):-
		dlg_messagebox(
			"���������",
			"���������� ��������� �� �������",
			mesbox_iconInformation,
			mesbox_buttonsOK, 
			mesbox_defaultFirst,
			mesbox_suspendApplication
		),
		!.
	qv(10,T1,_):-
		objects(_,Name,L),
		rvn(T1,L),
		rvn(L,T1),
		format(S,"%", Name),
		dlg_messagebox(
			"���������",
			S,
			mesbox_iconInformation,
			mesbox_buttonsOK,
			mesbox_defaultFirst,
			mesbox_suspendApplication
		),
		!.
  	qv(N,T1,T2):-
		ex_in_lst(N,T1),
		N1=N+1,
		qv(N1,T1,T2),!.
	qv(N,T1,T2):-
		ex_in_lst(N,T2),
		N1=N+1,
		qv(N1,T1,T2),!.    
	qv(N,T1,T2):-
		prop(N,S),
    		Ch=dlg_messagebox(
    			"������",
    			S,
    			mesbox_iconQuestion,
    			mesbox_buttonsYesNo,
    			mesbox_defaultFirst,
    			mesbox_suspendApplication
    			),
    		next(Ch,T1,T2,N).
	%���������� ������ ������������ �� �������� ������� 
	next(1,T1,T2,N):-
		N1=N+1, 
		qv(N1,[N|T1],T2),!.
	next(2,T1,T2,N):-
		N1=N+1,
		qv(N1,T1,[N|T2]),!.
	%�������� �� ������� �������� � ������
	ex_in_lst(_,[]):-
		!,fail.
	ex_in_lst(X,[X|_]):-
		!.
	ex_in_lst(X,[_|T]):-
		ex_in_lst(X,T).
	%�������� ������������, ��� ��� �������� 1-�� ������ ���������� �� ������
	rvn([],_):-
		!.
	rvn([H|T],L):-
		ex_in_lst(H,L),
		rvn(T,L).